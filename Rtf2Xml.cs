using System;
using System.IO;
using System.Xml;

namespace Notabene2NITF {
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1 {
		static void Main(string[] args) {
			//
			// TODO: Add code to start application here
			//

			string file_in = "";
			
			int i = args.Length;
			if (i == 0) {
				// Console.WriteLine("Input file- and output file-names, must be as command-line parameter 1 and 2!");
				// return;
				file_in = "test.rtf";
			} else {
				file_in = args[0]; 
			}

			string file_out = file_in + ".xml";
			if (i > 1) {
				file_out = args[1];
			}
	
			string rtf = "";
			read_rtf(file_in, ref rtf);
			string styles = get_styles(ref rtf);
			string body = get_body(ref rtf); 
		
			int table_start = 1;
			string table = "";
			while (table_start > 0)
				table += get_table(ref body, ref table_start);
		
			body = "<body>\r\n" + StripRtf(ref body) + "\r\n" + table + "</body>";
			write_string(file_out, ref body);
		}

		static void read_rtf(string file, ref string rtf) {
			TextReader tr = File.OpenText(file);
			rtf = tr.ReadToEnd(); 
			tr.Close();
		}

		static void write_string(string file, ref string rtf) {
			TextWriter tw = File.CreateText(file);
			tw.Write(rtf);
			tw.Close();
		}

		static string get_styles(ref string rtf) {
			int i1 = rtf.IndexOf(@"{\stylesheet",0);
			int i2 = rtf.IndexOf("}}",i1); 
			return rtf.Substring(i1, i2 - i1 + 2);
		}

		static string get_body(ref string rtf) {
			int i1 = rtf.IndexOf(@"\pard", 0);
			return rtf.Substring(i1);
		}

		public static string get_table(ref string str, ref int start) {
			//int i1 = str.IndexOf(@"\trowd ", start);
			// \intbl
			int i1 = str.IndexOf(@"\intbl", start);
			if (i1 == -1) {
				start = -1;
				return "";
			}

			int i2 = str.IndexOf(@"\lastrow ", i1 + 7) + 9;
			int i3 = str.IndexOf(@"\row }", i2) + 6;
			start = i3;
			string table = str.Substring(i1, i3 - i1);
			
			int start_row = 0;
			string rows = "";
			while (start_row > -1)
				rows += get_row(ref table, ref start_row);
			
			table = "<table>\r\n"
				+ rows
				+ "</table>\r\n";
			return table;
		}

		public static string get_row(ref string str, ref int start) {
			int i1 = str.IndexOf(@"\intbl", start);
			if (i1 == -1) {
				start = -1;
				return "";
			}

			int i2 = str.IndexOf(@"{\insrsid", i1 + 5) + 9;
			i2 = str.IndexOf(@" ", i2) + 1;
			int i3 = str.IndexOf(@"\cell }", i1);
			
			string row1 = str.Substring(i2, i3 - i2);
			string row2 = "<tr><td>" + row1.Replace("\\cell ", "</td><td>") +"</td></tr>\r\n" ; 
			row1 = StripRtf(ref row2);

			int i4 = str.IndexOf(@"\row }", i3) + 6 ;
			start = i4;
			return row1;
		}

		public static string StripRtf(ref string str) {
			int i1,  i2 = 0;
			string str2 = str;

			i1 = str.IndexOf(@"\", 0);
			while (i1 > -1) {
				i2 = str2.IndexOf(" ", i1 + 1);
				//if 
				str2 = str2.Substring(0, i1) + str2.Substring(i2 + 1);
				i1 = str2.IndexOf(@"\", i1);
			}

			string str1 = str2.Replace("}", ""); 
			str2 = str1.Replace("{", ""); 
			return str2;
		}
	}
}