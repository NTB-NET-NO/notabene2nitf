<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="ISO-8859-1" standalone="yes" indent="yes"/>

<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til NITF
	Felles mal for NTIF-Header
	Opprettet Av Roar Vestre 27.11.2001
	Sist endret Av Roar Vestre 14.03.2002: Lagt til "meta"-tagger
	Sist endret Av Roar Vestre 08.08.2003: Lagt til <meta name="NTBSendTilDirekte"/>
-->

<xsl:template name="messageHead"> 

<head>
<title><xsl:value-of select="subject"/></title>

<meta name="ntb-folder"><xsl:attribute name="content"><xsl:value-of select="adm/folder"/></xsl:attribute></meta>
<meta name="ntb-date"><xsl:attribute name="content"><xsl:value-of select="adm/ntb-date"/></xsl:attribute></meta>
<meta name="ntb-filename"><xsl:attribute name="content"><xsl:value-of select="adm/filename"/></xsl:attribute></meta>
<xsl:if test="sms[.!='']">
	<meta name="ntb-sms"><xsl:attribute name="content"><xsl:value-of select="sms" disable-output-escaping="yes"/></xsl:attribute></meta>
</xsl:if>
<xsl:if test="adm/ntb-lyd[.!='']">
	<meta name="ntb-lyd"><xsl:attribute name="content">
	<xsl:value-of select="adm/ntb-lyd" />
	</xsl:attribute>
	</meta>
	<meta name="ntb-lydfil"><xsl:attribute name="content">
	<xsl:value-of select="adm/ntb-lydfil" />
	</xsl:attribute>
	</meta>
</xsl:if>

<meta name="ntb-distribusjonsKode"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBDistribusjonsKode']/value"/></xsl:attribute></meta>
<meta name="ntb-kanal"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBKanal']/value"/></xsl:attribute></meta>
<meta name="ntb-meldingsSign"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsSign']/value"/></xsl:attribute></meta>
<meta name="ntb-meldingsType"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsType']/value"/></xsl:attribute></meta>
<meta name="ntb-bilderAntall"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBBilderAntall']/value"/></xsl:attribute></meta>
<meta name="ntb-id"><xsl:attribute name="content"><xsl:value-of select="adm/ntb-id"/></xsl:attribute></meta>
<meta name="ntb-meldingsVersjon"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsVersjon']/value"/></xsl:attribute></meta>
<meta name="ntb-utDato"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBUtDato']/value"/></xsl:attribute></meta>
<meta name="NTBSendTilDirekte"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBSendTilDirekte']/value"/></xsl:attribute></meta>

<docdata>
	<evloc><xsl:attribute name="county-dist"><xsl:value-of select="fields/field[name='NTBFylker']/value"/></xsl:attribute><xsl:value-of select="fields/field[name='NTBOmraader']/value"/></evloc>

	<doc-id regsrc="NTB">
		<xsl:attribute name="id-string"><xsl:value-of select="fields/field[name='NTBID']/value"/></xsl:attribute>
	</doc-id>

	<urgency>
		<xsl:attribute name="ed-urg"><xsl:value-of select="fields/field[name='NTBPrioritet']/value"/></xsl:attribute>
	</urgency>

	<date.issue>
		<xsl:attribute name="norm"><xsl:value-of select="adm/date.issue"/></xsl:attribute>
	</date.issue>

	<ed-msg>
		<xsl:attribute name="info"><xsl:value-of select="fields/field[name='NTBBeskjedTilRed']/value"/></xsl:attribute>
	</ed-msg>

	<du-key>
		<xsl:attribute name="version"><xsl:value-of select="fields/field[name='NTBMeldingsVersjon']/value + 1"/></xsl:attribute>
		<xsl:attribute name="key"><xsl:value-of select="subject"/></xsl:attribute>
	</du-key>

	<doc.copyright>
		<xsl:attribute name="year"><xsl:value-of select="adm/year"/></xsl:attribute>
		<xsl:attribute name="holder">NTB</xsl:attribute>
	</doc.copyright>

	<key-list>
		<keyword>
			<xsl:attribute name="key"><xsl:value-of select="fields/field[name='NTBStikkord']/value"/></xsl:attribute>
 		</keyword>
	</key-list>
</docdata>

<pubdata>
	<xsl:attribute name="date.publication"><xsl:value-of select="adm/timenitf"/></xsl:attribute>
	<xsl:attribute name="item-length"><xsl:value-of select="string-length(/message/xmltext)"/></xsl:attribute>
	<xsl:attribute name="unit-of-measure">character</xsl:attribute>
</pubdata>

<revision-history>
	<xsl:attribute name="name"><xsl:value-of select="fields/field[name='NTBMeldingsSign']/value"/></xsl:attribute>
</revision-history>

<tobject>
	<xsl:attribute name="tobject.type"><xsl:value-of select="group"/></xsl:attribute>
	<tobject.property>
		<xsl:attribute name="tobject.property.type"><xsl:value-of select="subgroup"/></xsl:attribute>
	</tobject.property>

	<xsl:for-each select="categories/tobject.subject">
		<!--<tobject.subject tobject.subject.refnum="15000000" tobject.subject.code="SPO" tobject.subject.type="Sport" />-->
		<tobject.subject>
  			<xsl:attribute name="tobject.subject.refnum"><xsl:value-of select="@tobject.subject.refnum"/></xsl:attribute>
  			<xsl:attribute name="tobject.subject.code"><xsl:value-of select="@tobject.subject.code"/></xsl:attribute>
  			<xsl:attribute name="tobject.subject.type"><xsl:value-of select="@tobject.subject.type"/></xsl:attribute>
  		<xsl:choose>
  		<xsl:when test="@tobject.subject.code='KUL'">
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="../../subcategory/field[name='NTBUnderKatKultur']/value"/>
  			</xsl:attribute>
  		</xsl:when>
  		<xsl:when test="@tobject.subject.code='OKO'">
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="../../subcategory/field[name='NTBUnderKatOkonomi']/value"/>
  			</xsl:attribute>
  		</xsl:when>
  		<xsl:when test="@tobject.subject.code='KUR'">
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="../../subcategory/field[name='NTBUnderKatKuriosa']/value"/>
  			</xsl:attribute>
  		</xsl:when>
  		<xsl:when test="@tobject.subject.code='SPO'">
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="../../subcategory/field[name='NTBUnderKatSport']/value"/>
  			</xsl:attribute>
  		</xsl:when>
  		</xsl:choose>
  		</tobject.subject>
	</xsl:for-each>	

</tobject>

</head>

</xsl:template> 
</xsl:stylesheet> 