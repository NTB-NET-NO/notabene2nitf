'TransForm.vb
'
'Last Changed 2003.04.07, by Roar Vestre, NTB: Errorhandling of ChangeXmlAdm and for wrong NTBID inside ChangeXmlAdm 
'
Imports System.Xml
Imports System.IO
Imports ntb_FuncLib
Imports Notabene2NITF

Public Class TransForm

    Private Shared xsltProc As Xsl.XslTransform
    Private Shared xchgCategory As XmlXchange = New XmlXchange()
    Private Shared xchgSubCategory As XmlXchange = New XmlXchange()

    ' XSLT Problem workaround:        
    Private Shared xslt4Proc As MSXML2.IXSLProcessor

    Public Shared Sub LoadXmlFiles(ByVal xslFilePath As String)
        xsltProc = New Xsl.XslTransform()

        xsltProc.Load(xslFilePath & "\" & "xml2nitf.xsl")
        xchgCategory.LoadXml(xslFilePath & "\" & "ReplaceCategory.xml")
        xchgSubCategory.LoadXml(xslFilePath & "\" & "ReplaceSubCategory.xml")

        ' XSLT Problem workaround, using MSXML4.DLL instead of .NET-xml components :       
        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate()
        xsltDoc = New MSXML2.FreeThreadedDOMDocument()

        xsltDoc.load("xml2nitf.xsl")
        xmlError = xsltDoc.parseError

        If (xmlError.errorCode <> 0) Then
            'MsgBox(xmlError.reason & " i linje: " & xmlError.line)
        End If
        xslt.stylesheet = xsltDoc
        xslt4Proc = xslt.createProcessor
        ' End XSLT Problem workaround:        

    End Sub

    Public Shared Function GetXml(ByRef strFileName As String, ByRef strNitfPath As String, ByRef strXMLTempPath As String, ByRef dtTimeStamp As DateTime, ByRef errImport As Exception, ByVal withNitfSub As Boolean, ByVal errorNitfPath As String, ByVal blnDebugXml As Boolean) As String
        ' ByRef xmlDoc As XmlDocument,
        Dim xmlDoc As XmlDocument = New XmlDocument()

        Dim strFilePath As String
        ' declare the StreamReader, for accessing the file

        'Dim strWriter As StringWriter = New StringWriter()
        'Console.SetOut(strWriter)

        'Try
        Dim srXml As StreamReader = New StreamReader(strFileName, Text.Encoding.GetEncoding("iso-8859-1"))
        '  File.OpenText(strFileName)

        Dim strRaw As String
        Dim strRtf As String
        Dim strXmlHead As String
        Dim strXml As String
        Dim strXmlBody As String
        Dim intTemp As Long
        Dim intRtfStart As Integer

        strRaw = srXml.ReadToEnd
        srXml.Close()

        If strRaw.IndexOf("<folder><![CDATA[Ut-Tekstarkiv]]></folder>") > -1 Then
            ' dtTimeStamp = "1000.01.01"
            Return "skip"
        End If

        ' Read entire File in Notabene Distributor's XML-RTF-format
        intRtfStart = strRaw.IndexOf("<rtftext>")
        If intRtfStart = -1 Then
            Return "error"
        End If

        ' Get Header part (pure XML)
        strXmlHead = strRaw.Substring(0, intRtfStart - 1)

        CleanCodes(strXmlHead)

        ' Get RTF-body part (RTF format)
        strRtf = strRaw.Substring(intRtfStart)

        ' Convert RTF-bodypart to XML
        strXmlBody = Rtf2Xml.ConvertRtf(strRtf)

        ' Make Complete XML-text string in Intermediate XML-format
        strXml = strXmlHead & strXmlBody & "</message>"

        Try
            xmlDoc.LoadXml(strXml)
            ' Change XML adm-headers with different date-fomats, and get dtTimeStamp
            dtTimeStamp = ChangeXmlAdm(xmlDoc)
        Catch err As Exception
            LogFile.WriteFile(errorNitfPath & "\" & Path.GetFileName(strFileName) & ".xml", strXml)
            errImport = err
            Return "error"
        End Try

        ' For debug purposes only:
        If blnDebugXml Then
            ' Write Intermediate XML-format
            xmlDoc.Save(strXMLTempPath & "\" & Path.GetFileName(strFileName))
        End If

        strFilePath = strNitfPath & "\" & Path.GetFileName(strFileName)

        If withNitfSub Then
            strFilePath = FuncLib.MakeSubDirDate(strFilePath, dtTimeStamp)
        End If

        Try
            Dim xTw As XmlTextWriter = New XmlTextWriter(strFilePath, Text.Encoding.GetEncoding("iso-8859-1"))
            xTw.Formatting = Formatting.Indented

            xTw.IndentChar = vbTab
            xTw.Indentation = 1

            ' XSLT Problem workaround:        
            Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument()
            xml4Doc.loadXML(xmlDoc.OuterXml)

            xslt4Proc.input = xml4Doc
            xslt4Proc.transform()
            Dim strXml4 As String = xslt4Proc.output()
            xTw.WriteRaw(Replace(strXml4, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1))
            ' End XSLT Problem workaround

            ' If no Workaround, uncomment:
            'xTw.WriteStartDocument(True)
            'xsltProc.Transform(xmlDoc, Nothing, xTw)
            xTw.Flush()
            xTw.Close()
        Catch err As Exception
            errImport = err
            Return "hold"
        End Try
        Return "ok"
    End Function

    Private Shared Sub CleanCodes(ByRef str2 As String)
        'str2 = str2.Replace("&", "&amp;")
        str2 = str2.Replace(ChrW(148), """")
        str2 = str2.Replace(ChrW(147), """")
        str2 = str2.Replace(ChrW(150), "-")
        str2 = str2.Replace(ChrW(140), "&#140;")
        str2 = str2.Replace(ChrW(130), "&#130;")
        str2 = str2.Replace(ChrW(131), "&#131;")
    End Sub

    Private Shared Function ChangeXmlAdm(ByRef xmlDoc As XmlDocument) As String
        'Try
        'Create a new nodes.
        Dim nodeRoot As XmlNode = xmlDoc.SelectSingleNode("/message/adm")
        Dim node As XmlNode
        Dim elem As XmlElement

        node = xmlDoc.SelectSingleNode("/message/adm/timestamp")
        Dim dtTimeStamp As DateTime = node.InnerText
        'Dim dtTimeStamp As String = node.InnerText

        'Add new node to the document.
        elem = xmlDoc.CreateElement("timenitf")
        elem.InnerText = Format(dtTimeStamp, "yyyyMMddTHHmmss")
        nodeRoot.AppendChild(elem)

        'Add new node to the document.
        elem = xmlDoc.CreateElement("date.issue")
        elem.InnerText = Format(dtTimeStamp, "yyyyMM")
        nodeRoot.AppendChild(elem)

        'Add new node to the document.
        elem = xmlDoc.CreateElement("ntb-date")
        elem.InnerText = Format(dtTimeStamp, "dd.MM.yyyy HH:mm")
        nodeRoot.AppendChild(elem)

        ' Strip filename
        node = xmlDoc.SelectSingleNode("/message/adm/filename")
        node.InnerText = Path.GetFileName(node.InnerText)

        ' SoundFilename
        node = xmlDoc.SelectSingleNode("/message/lyd")
        If node.InnerText <> "" Then
            ' Stripped sound file name
            elem = xmlDoc.CreateElement("ntb-lyd")
            elem.InnerText = Path.GetFileNameWithoutExtension(node.InnerText)
            nodeRoot.AppendChild(elem)

            ' Full sound file name with local path at NTB
            elem = xmlDoc.CreateElement("ntb-lydfil")
            elem.InnerText = node.InnerText
            nodeRoot.AppendChild(elem)
        End If

        'Catch E As Exception
        'Console.WriteLine("There was an error reading the file words.txt. Please ensure it is in the right directory")
        'End Try

        ' Get Right Categories
        node = xmlDoc.SelectSingleNode("/message/category")
        nodeRoot = xmlDoc.SelectSingleNode("/message")
        If node.InnerText <> "" Then
            elem = xmlDoc.CreateElement("categories")
            elem.InnerXml = xchgCategory.ParseCategory(node.InnerText)
            nodeRoot.AppendChild(elem)
        End If

        ' Get SubCategories Names
        Dim nodes As XmlNodeList = xmlDoc.SelectNodes("/message/subcategory/field/value")
        For Each node In nodes
            If node.InnerText <> "" Then
                node.InnerText = xchgSubCategory.ParseSubCat(node.InnerText)
            End If
        Next

        ' Get Evloc (NTBOmraader) Names
        node = xmlDoc.SelectSingleNode("/message/fields/field[name='NTBOmraader']/value")
        If Not node Is Nothing Then
            If node.InnerText <> "" Then
                node.InnerText = xchgSubCategory.ParseSubCat(node.InnerText)
            End If
        End If

        ' Get County-dist (NTBFylker) Names
        node = xmlDoc.SelectSingleNode("/message/fields/field[name='NTBFylker']/value")
        If Not node Is Nothing Then
            If node.InnerText <> "" Then
                node.InnerText = xchgSubCategory.ParseSubCat(node.InnerText)
            End If
        End If

        Try
            ' Make new message-id
            node = xmlDoc.SelectSingleNode("/message/fields/field[name='NTBID']/value")
            If Not node Is Nothing Then
                nodeRoot = xmlDoc.SelectSingleNode("/message/adm")
                If node.InnerText <> "" Then
                    elem = xmlDoc.CreateElement("ntb-id")
                    Dim strNtbId As String = node.InnerText
                    elem.InnerText = strNtbId.Substring(0, strNtbId.LastIndexOf("_"))
                    nodeRoot.AppendChild(elem)
                End If
            End If
        Catch
            elem = xmlDoc.CreateElement("ntb-id")
            elem.InnerText = "Error_" & Format(Now, "yyyyMMdd_HHmmss")
            nodeRoot.AppendChild(elem)
        End Try

        Return dtTimeStamp

    End Function

End Class

