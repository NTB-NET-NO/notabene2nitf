Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports ntb_FuncLib

Module MainDebug
    ' Debug version of main program not running as service
    Const TEXT_STARTED As String = "NTB_Notabene2NITF Service Started"

    '*** Global variables:
    Private NitfPath As String
    Private errorNitfPath As String
    Private holdNitfPath As String
    Private errFilePath As String
    Private logFilePath As String
    Private withNitfSub As Boolean

    '*** Module variables:
    Private importXMLPath As String
    Private XMLDonePath As String
    Private XMLTempPath As String
    Private blnDebugXml As Boolean

    'Public FSW1 As FileSystemWatcher
    Friend WithEvents FSW1 As System.IO.FileSystemWatcher

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

    'Public Sub Main()
    '    ServiceBase.Run(New SimpleService())
    'End Sub

    Sub Main()
        ' Debug version
        InitConfigValues()
        ImportAll()
    End Sub

    'Public Sub New()
    '    MyBase.New()
    '    CanPauseAndContinue = True
    '    ServiceName = "NTB_Notabene2NITF"
    'End Sub

    'Protected Overrides Sub OnStop()
    '    FSW1.EnableRaisingEvents = False
    '    Sleep(5000)
    '    LogFile.WriteLog(logFilePath, "NTB_Notabene2NITF Service stopped")
    '    EventLog.WriteEntry("NTB_Notabene2NITF Service stopped")
    'End Sub

    'Protected Overrides Sub OnPause()
    '    FSW1.EnableRaisingEvents = False
    '    LogFile.WriteLog(logFilePath, "NTB_Notabene2NITF Service paused")
    '    EventLog.WriteEntry("NTB_Notabene2NITF Service paused")
    'End Sub

    'Protected Overrides Sub OnContinue()
    '    LogFile.WriteLog(logFilePath, "NTB_Notabene2NITF Service continued")
    '    EventLog.WriteEntry("NTB_Notabene2NITF Service continued")
    '    StartJob()
    'End Sub

    'Protected Overrides Sub OnStart(ByVal args() As String)
    '    Try
    '        FSW1 = New FileSystemWatcher()

    '        '*** Init global variables
    '        InitConfigValues()

    '        '*** Enable filesystem watcher for Filecreation interrupt
    '        FSW1.IncludeSubdirectories = False
    '        FSW1.Filter = "*.xml"
    '        FSW1.Path = importXMLPath

    '        LogFile.WriteLog(logFilePath, TEXT_STARTED)
    '        EventLog.WriteEntry(TEXT_STARTED)

    '        StartJob()

    '    Catch err As Exception
    '        Dim strErr As String = "Error in Initialization, program aborted"
    '        LogFile.WriteErr(errFilePath, strErr, err)
    '        EventLog.WriteEntry(strErr)
    '        ' End Program
    '        End
    '    End Try

    'End Sub

    Private Sub InitConfigValues()
        ' Reading Config values from "Notabene2NITF.exe.config" file

        importXMLPath = AppSettings("importXMLPath")
        XMLDonePath = AppSettings("XMLDonePath")
        XMLTempPath = AppSettings("XMLTempPath")
        NitfPath = AppSettings("NitfPath")

        errorNitfPath = AppSettings("errorNitfPath")
        holdNitfPath = AppSettings("holdNitfPath")
        withNitfSub = (AppSettings("withNitfSub") = "yes")

        logFilePath = AppSettings("logFilePath")
        errFilePath = AppSettings("errFilePath")
        blnDebugXml = (AppSettings("DebugXml") = "yes")

        Dim xslFilePath As String = AppSettings("xslFilePath")

        LogFile.MakePath(importXMLPath)
        LogFile.MakePath(XMLDonePath)
        LogFile.MakePath(XMLTempPath)
        LogFile.MakePath(NitfPath)
        LogFile.MakePath(errorNitfPath)
        LogFile.MakePath(holdNitfPath)
        LogFile.MakePath(logFilePath)
        LogFile.MakePath(errFilePath)
        TransForm.LoadXmlFiles(xslFilePath)
    End Sub

    Private Sub StartJob()
        ' Start service
        ' All files waiting in import folder will be imported:
        ImportAll()

        ' "Sub FSW1_Created" will now trigger on Filesystem event: Created
        ' and do all new files appearing from now on:
        FSW1.EnableRaisingEvents = True

    End Sub

    Sub FSW1_Created(ByVal sender As Object, ByVal ev As System.IO.FileSystemEventArgs) Handles FSW1.Created
        ' Disable events to prevent multi-threading conflicts!
        FSW1.EnableRaisingEvents = False

        Console.WriteLine(Now & ": New File Created: " & ev.Name)
        Sleep(2000)

        Try
            ImportOneFile(ev.FullPath)
        Catch e As Exception
            LogFile.WriteErr(errFilePath, "Error open SQL-server", e)
        End Try

        ' Enable events when all files have been imported
        FSW1.EnableRaisingEvents = True
        ' Extra importjob in case of more files arrived since last event
        ImportAll()

    End Sub

    Sub ImportAll()
        ' Process the list of files found in the directory
        Dim fileEntries As String() = Directory.GetFiles(importXMLPath)

        If fileEntries.Length = 0 Then
            Return
        End If

        '*** Wait 3 seconds for all files to be written
        Sleep(3000)

        Dim strFileName As String
        For Each strFileName In fileEntries
            ImportOneFile(strFileName)
        Next

    End Sub

    Sub ImportOneFile(ByRef strFileName As String)
        ' Process the list of files found in the directory

        Dim strStatus As String
        Dim errImport As Exception
        Dim dtTimeStamp As DateTime

        strStatus = TransForm.GetXml(strFileName, NitfPath, XMLTempPath, dtTimeStamp, errImport, withNitfSub, errorNitfPath, blnDebugXml)

        Select Case strStatus
            ' Logging and handling of source files:
        Case "ok"
                LogFile.WriteLog(logFilePath, "File OK: " & strFileName)
                ' File is imported OK
                Try
                    'File.Move(strFileName, FuncLib.MakeSubDirDate(XMLDonePath & "\" & Path.GetFileName(strFileName), dtTimeStamp))
                    File.Copy(strFileName, FuncLib.MakeSubDirDate(XMLDonePath & "\" & Path.GetFileName(strFileName), dtTimeStamp), True)
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "Error in Moving file: " & strFileName, e)
                End Try
                Console.WriteLine("File OK: " & strFileName)
            Case "error"
                ' File NOT imported because of ERROR, error  
                LogFile.WriteErr(errFilePath, "ERROR importing:" & strFileName, errImport)
                Try
                    File.Move(strFileName, errorNitfPath & "\" & Path.GetFileName(strFileName))
                    'File.Copy(strFileName, errorNitfPath & "\" & Path.GetFileName(strFileName))
                    'File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving file
                    LogFile.WriteErr(errFilePath, "ERROR moving file:" & strFileName, e)
                End Try
            Case "skip"
                Try
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "Error in Deleting file: ", e)
                End Try
            Case "hold"
                Try
                    File.Move(strFileName, holdNitfPath & "\" & Path.GetFileName(strFileName))
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "Error in Deleting file: ", e)
                End Try
            Case Else

        End Select

    End Sub

End Module
