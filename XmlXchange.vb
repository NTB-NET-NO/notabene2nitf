Imports System.Xml

Public Class XmlXchange

    Private htTextReplace As Hashtable = New Hashtable()

    Public Sub LoadXml(ByVal strFile As String)
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim nodeListText As XmlNodeList
        Dim strFind, strReplace As String

        xmlDoc.Load(strFile)
        nodeListText = xmlDoc.SelectNodes("//node")

        Dim node As XmlNode
        For Each node In nodeListText
            strFind = LCase(node.ChildNodes(0).InnerText)
            strReplace = node.ChildNodes(1).InnerText
            htTextReplace.Add(strFind, strReplace)
        Next
    End Sub

    Public Function ParseCategory(ByRef strInput As String) As String
        Dim strKey As String
        Dim strKey2 As String
        Dim strConf As Array = Split(strInput, ";")
        Dim strResult As String

        Array.Sort(strConf)

        For Each strKey In strConf
            strKey = LCase(strKey)
            If strKey <> "" And strKey <> strKey2 Then
                strResult &= htTextReplace(strKey)
            End If
            strKey2 = strKey
        Next

        Return strResult
    End Function

    Public Function ParseSubCat(ByRef strInput As String) As String
        Dim strKey As String
        Dim strKey2 As String
        Dim strConf As Array = Split(strInput, ";")

        Dim strResult As String
        Dim strValue As String

        Array.Sort(strConf)

        For Each strKey In strConf
            strKey = LCase(strKey)
            If strKey <> "" And strKey <> strKey2 Then
                strValue = htTextReplace(strKey)
                If strValue = "" Then
                    strValue = strKey
                End If
                strResult &= strValue & ";"
            End If
            strKey2 = strKey
        Next

        Return strResult
    End Function


End Class
