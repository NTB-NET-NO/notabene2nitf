<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="ISO-8859-1" standalone="yes" indent="yes"/>

<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til NITF
	Sist endret Av Roar Vestre 14.08.2001
-->
<xsl:include href="nitfhead.xsl" />

<xsl:template match="/message"> 
<nitf version="-//IPTC-NAA//DTD NITF-XML 2.5//EN" change.date="9 august 2000" change.time="1900" baselang="no-NO">

<xsl:call-template name="messageHead"/> 

<body>
<body.head>
<distributor>NTB</distributor>
<hedline>
	<hl1>
		<xsl:choose>
		<xsl:when test='xmltext/_infolinje/p'>
			<xsl:value-of select="xmltext/_infolinje/p" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
			<xsl:when test="string-length(xmltext/_infolinje/p) &gt; 80">
				<xsl:value-of select="xmltext//p" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring(xmltext//p, 1, 80)" />....				
			</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
		</xsl:choose>
	</hl1>
</hedline>

<xsl:if test="xmltext/_byline">
	<byline>

	<xsl:for-each select="xmltext/_byline/p">
		<xsl:copy-of select="."/>
	</xsl:for-each>
	</byline>
</xsl:if>

</body.head>
<body.content>
	
<xsl:call-template name="xmltext" />

<xsl:call-template name="media" />

</body.content>

<!--
<sms>
	<xsl:value-of select="sms" disable-output-escaping="yes"/>
</sms>
-->

<body.end>
	<tagline>  
		<xsl:for-each select="fields/field[name='NTBEpostAdresse']">
			<a>
				<xsl:attribute name="href">mailto:<xsl:value-of select="value"/></xsl:attribute>
				<xsl:value-of select="value"/>
			</a>
		</xsl:for-each>
	</tagline>
</body.end>

</body>
</nitf>
</xsl:template>

<xsl:template name="xmltext">
	<xsl:apply-templates select="xmltext" />
</xsl:template> 
 
<xsl:template match="*/p">
	<p><xsl:value-of select="."/></p>
</xsl:template> 

<xsl:template match="_brodtekst/p">
	<p><xsl:value-of select="."/></p>
</xsl:template> 

<xsl:template match="_brodtekst_innrykk/p">
	<p>
		<xsl:attribute name="innrykk">true</xsl:attribute>  	
		<xsl:value-of select="."/>
	</p>
</xsl:template> 

<xsl:template match="_infolinje">
	<!-- Empty Template -->
</xsl:template>

<xsl:template match="_ingress/p">
	<p lede="true">
		<xsl:value-of select="."/>
	</p>
</xsl:template> 

<xsl:template match="_tabellkode/p">
	<p style="tabellkode">
		<xsl:value-of select="."/>
	</p>
</xsl:template> 

<xsl:template match="_mellomtittel/p">
	<hl2><xsl:value-of select="."/></hl2>
</xsl:template> 

<xsl:template match="xmltext/_byline/p">
	<!-- empty -->
</xsl:template> 

<xsl:template match="table">
<!-- <xsl:copy-of select="." /> -->
	<table>
	Test: <xsl:value-of select="./..//p"/>
	<xsl:attribute name="class">
		<xsl:value-of select="substring(../_tabellkode/p, 2 ,string-length(../_tabellkode/p) - 2)"/>
	</xsl:attribute>
	<xsl:apply-templates select="tr" />
	</table>
</xsl:template>

<xsl:template match="tr">
<!-- <xsl:copy-of select="." /> -->
	<tr>
	<xsl:apply-templates select="td" />
	</tr>
</xsl:template>

<xsl:template match="td">
<!-- <xsl:copy-of select="." /> -->
	<td>
	<xsl:choose>
	<xsl:when test="position() = 1">
		<xsl:attribute name="align">left</xsl:attribute>
	</xsl:when>

	<xsl:when test=". = '-'">
		<xsl:attribute name="align">center</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
		<xsl:attribute name="align">right</xsl:attribute>
	</xsl:otherwise>
	</xsl:choose>
		<xsl:value-of select="."/>
	</td>
</xsl:template>

<!--
-->

<xsl:template name="media">
	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '1']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr1']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst1']/value"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '2']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr2']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst2']/value"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '3']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr3']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst3']/value"/>
 		</media-caption>
	</media>
	</xsl:if>
		
	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '4']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr4']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst4']/value"/>
 		</media-caption>
	</media>
	</xsl:if>
		
	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '5']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr5']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst5']/value"/>
 		</media-caption>
	</media>
	</xsl:if>
		
	<xsl:if test="fields/field[name='NTBBilderAntall']/value[. &gt;= '6']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="fields/field[name='NTBBildeNr6']/value"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="fields/field[name='NTBBildeTekst6']/value"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="soundsource">
		<media media-type="audio">
			<media-reference mime-type="application/x-shockwave-flash">
			<xsl:attribute name="source">http://www.soundonweb.com/soundftp/flash/<xsl:value-of select="soundsource"/>.swf</xsl:attribute>
			</media-reference>
			<media-reference mime-type="image/gif" source="http://www.soundonweb.com/soundftp/grafikk/ntb.gif"/>
			<media-reference mime-type="text/javascript" source="http://www.soundonweb.com/soundftp/flashsound.js"/>
		</media>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>