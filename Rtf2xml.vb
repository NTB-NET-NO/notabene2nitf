Imports System
Imports System.IO
Imports System.Text
Imports System.Xml

Public Class Rtf2Xml

    Public Shared htStyles As Hashtable

    Enum ParType
        Finish = 0
        Para = 1
        Table = 2
    End Enum

    Public Shared Function ConvertRtf(ByRef strRtf As String) As String

        FillStyles(strRtf)
        Dim strBody As String = GetBody(strRtf)
        Dim parCount As Integer = 0
        Dim strTableType As String = ""
        CleanCodes(strBody)
        strBody = StripRtfCodes(strBody, "{\field")

        Dim intStart As Integer = 0

        Dim strXml As String = ""

        Do
            parCount += 1
            Select Case GetParType(strBody, intStart)
                Case ParType.Para
                    'While (intStart > -1)
                    strXml &= GetPar(strBody, intStart, parCount, strTableType)
                    'End While
                Case ParType.Table
                    'While (intStart > -1)
                    strXml &= GetTable(strBody, intStart, strTableType)
                    'End While
                Case ParType.Finish
                    Exit Do
                Case Else
                    Exit Do
            End Select
        Loop

        Return "<xmltext>" & vbCrLf & strXml & "</xmltext>"
    End Function

    Private Shared Sub CleanCodes(ByRef str2 As String)
        str2 = str2.Replace(vbCrLf, "")
        str2 = str2.Replace("&", "&amp;")
        str2 = str2.Replace(Chr(0), "")
        str2 = str2.Replace("]]></rtftext>", "")
        str2 = str2.Replace("</message>", "")
        'str2 = str2.Replace("'", "&apos;")
        'str2 = str2.Replace(ChrW(148), "&quot;")
        'str2 = str2.Replace(ChrW(147), "&quot;")
        str2 = str2.Replace(ChrW(148), """")
        str2 = str2.Replace(ChrW(147), """")
        str2 = str2.Replace(ChrW(150), "-")
        str2 = str2.Replace(ChrW(140), "&#140;")
        str2 = str2.Replace(ChrW(130), "&#130;")
        str2 = str2.Replace(ChrW(131), "&#131;")

        str2 = str2.Replace("<", "&lt;")
        str2 = str2.Replace(">", "&gt;")
    End Sub

    Private Shared Function GetBody(ByRef rtf As String) As String
        'Dim i1 As Integer = rtf.IndexOf("\pard", 0)
        'Fix 02.07.2002 By RoV:
        Dim i1 As Integer = rtf.IndexOf("}}\pard", 0)
        Return rtf.Substring(i1)
    End Function

    Private Shared Function GetParType(ByRef strBody As String, ByRef intStart As Integer) As ParType
        ' Search RtfBody for Start of Paragraph or Table and returns ParType and StartPoint
        If intStart = -1 Then
            Return ParType.Finish
        End If

        'Dim intPar As Integer = strBody.IndexOf("\pard\plain \s", intStart)
        Dim intPar As Integer = strBody.IndexOf("\pard\plain ", intStart)

        Dim intTable As Integer = strBody.IndexOf("\trowd ", intStart)

        If intTable = -1 And intPar = -1 Then
            intStart = -1
            Return ParType.Finish
        ElseIf intTable = -1 Then
            intStart = intPar
            Return ParType.Para
        ElseIf intPar = -1 Then
            intStart = intTable
            Return ParType.Table
        ElseIf intTable - intPar < 0 Then
            intStart = intTable
            Return ParType.Table
        Else
            intStart = intPar
            Return ParType.Para
        End If

    End Function

    Private Shared Function GetPar(ByRef str As String, ByRef intStart As Integer, ByVal parCount As Integer, ByRef strTableType As String) As String

        Dim i1 As Integer = intStart

        ' Get end of Paragraph or start of new par
        Dim i2 As Integer = str.IndexOf("\pard\plain", i1 + 5)
        If i2 = -1 Then
            i2 = str.IndexOf("\par }}", i1)
            If i2 = -1 Then
                intStart = -1
                Return ""
            End If
        End If

        ' Get start of Table if before new paragraph (= end of this paragraph)
        Dim i3 As Integer = str.IndexOf("\trowd", i1)
        If i3 > -1 And i3 < i2 Then
            i2 = i3
        End If

        Dim strPar As String = str.Substring(i1, i2 - i1)
        Dim strStyle As String = GetStyle(strPar)
        If strStyle = "normal" Then
            strStyle = "_brodtekst_innrykk"
        End If

        ' Bugfix for Notabene-Articles with Wrong Styles:
        Dim strStyleReplace As String
        Dim blnReplace As Boolean = True
        If parCount = 1 And strStyle <> "_infolinje" Then
            strStyleReplace = "_infolinje"
        ElseIf parCount = 2 And strStyle <> "_ingress" And strStyle <> "_brodtekst" Then
            strStyleReplace = "_ingress"
        Else
            blnReplace = False
        End If

        strPar = "<p>" & strPar.Replace("\par ", "</p><p>") & "</p>"
        strPar = StripRtf(strPar)

        Dim intLast As Integer = strPar.LastIndexOf("<p></p>")
        If intLast > -1 Then
            strPar = strPar.Substring(0, intLast)
        End If

        If strStyle = "_tabellkode" Then
            'Get table code from paragraph 
            Dim intS1 As Integer = strPar.LastIndexOf("[")
            Dim intS2 As Integer = strPar.LastIndexOf("]")
            If intS1 > -1 And intS2 > -1 And intS2 > intS1 Then
                strTableType = strPar.Substring(intS1 + 1, intS2 - intS1 - 1)
            Else
                strTableType = ""
            End If
        Else
            strTableType = ""
        End If

        If blnReplace And strPar.IndexOf("</p><p>") > -1 Then
            strPar = "<" & strStyleReplace & ">" & Replace(strPar, "</p><p>", "</p></" & strStyleReplace & "><" & strStyle & "><p>", 1, 1) & "</" & strStyle & ">" & vbCrLf
        Else
            strPar = "<" & strStyle & ">" & strPar & "</" & strStyle & ">" & vbCrLf
        End If

        intStart = i2
        Return strPar

    End Function

    Private Shared Function GetStyle(ByRef strPar As String) As String
        Dim strStyle As String
        Dim intStilNr As Integer

        Dim i1 As Integer = strPar.IndexOf("\s", 0)
        If i1 = -1 Then
            Return "normal"
        End If

        Dim i2 As Integer = strPar.IndexOf("\", i1 + 2)
        If i2 = -1 Then
            Return ""
        End If
        i1 += 2
        Try
            intStilNr = Convert.ToInt32(strPar.Substring(i1, i2 - i1))
        Catch
            Return "normal"
        End Try

        strStyle = htStyles(intStilNr)
        If strStyle = "" Then
            Return "normal"
        Else
            Return strStyle
        End If

    End Function

    Private Shared Function GetTable(ByRef str As String, ByRef intStart As Integer, ByVal strTableType As String) As String

        Dim i1 As Integer = intStart
        Dim i2 As Integer = str.IndexOf("\par }", i1)

        If i2 = -1 Then
            intStart = -1
            Return ""
        End If

        Dim table As String = str.Substring(i1, i2 - i1)

        Dim startRow As Integer = 0
        Dim rows As String = ""
        While (startRow > -1)
            rows &= GetRow(table, startRow, i2)
        End While

        Dim i3 As Integer = table.LastIndexOf("\pard\plain \s")
        If i3 = -1 Then
            intStart = i2
        Else
            intStart = i3 + i1 'i2
        End If

        If strTableType <> "" Then
            strTableType = " class='" & strTableType & "'"
        Else
            strTableType = ""
        End If
        table = "<table" & strTableType & ">" & vbCrLf & rows & vbCrLf & "</table>" & vbCrLf

        Return table
    End Function

    Private Shared Function GetRow(ByRef str As String, ByRef start As Integer, ByRef intEnd As Integer) As String
        Dim i1 As Integer
        Dim i2 As Integer

        i1 = str.IndexOf("\intbl", start)
        If i1 = -1 Then
            i1 = str.IndexOf("\trowd", start)
            If i1 = -1 Then
                i1 = str.IndexOf("{\fs", start)
                If i1 = -1 Then
                    start = -1
                    Return ""
                End If
            End If
        End If

        i2 = str.IndexOf("\row }", i1)
        If i2 = -1 Then
            start = -1
            Return ""
        Else
            i2 += 6
        End If

        Dim row1 As String = str.Substring(i1, i2 - i1)
        Dim row2 As String = "<tr><td>" & row1.Replace("\cell ", "</td><td>") & "</td></tr>"
        row1 = StripRtf(row2)
        row2 = row1.Replace("</td><td></td></tr>", "</td></tr>")

        start = i2
        Return row2 & vbCrLf
    End Function

    Private Shared Function StripRtf(ByRef str As String) As String
        Dim i1 As Integer = 0
        Dim i2 As Integer = 0
        Dim str2 As String = str

        str2 = Replace(str2, "\rquote ", "&apos;")
        str2 = Replace(str2, "\endash ", "-")
        str2 = Replace(str2, "\-", "")
        str2 = Replace(str2, "\~", " ")

        str2 = Replace(str2, "\tab ", " <tab/>")
        'str2 = Replace(str2, "\tab ", " ")

        str2 = Replace(str2, "{\*\bkmkstart CursorPos}", "")
        str2 = Replace(str2, "{\*\bkmkend CursorPos}", "")

        i1 = str.IndexOf("\", 0)
        While (i1 > -1)
            i2 = str2.IndexOf(" ", i1 + 1)
            If i2 = -1 Then
                Exit While
            End If
            str2 = str2.Substring(0, i1) + str2.Substring(i2 + 1)
            i1 = str2.IndexOf("\", i1)
        End While

        str2 = str2.Replace("}", "")
        str2 = str2.Replace("{", "")

        Dim strNew As String = ""
        Dim i As Integer
        For i = 0 To str2.Length - 1
            If str2.Chars(i) >= Chr(0) And str2.Chars(i) < Chr(32) Then
                strNew &= "&#" & Asc(str2.Chars(i)) & ";"
            Else
                strNew &= str2.Chars(i)
            End If
        Next

        Return strNew
    End Function

    Shared Function StripRtfCodes(ByRef str1 As String, ByVal strFind As String) As String
        ' Strips off all rtf-code sets starting with: strFind (must include {) ending with last } in stack
        Dim i As Integer
        Dim intStart As Integer
        Dim strNew As String = str1

        Do
            intStart = strNew.IndexOf(strFind)

            If intStart = -1 Then
                Return strNew
            End If

            Dim intStack As Integer = 1
            For i = intStart + 1 To strNew.Length - 1
                If strNew.Chars(i) = "{" Then
                    intStack += 1
                ElseIf strNew.Chars(i) = "}" Then
                    intStack -= 1
                End If
                If intStack = 0 Then
                    i -= 1
                    Exit For
                End If
            Next
            strNew = strNew.Substring(0, intStart - 1) & strNew.Substring(i)
        Loop

    End Function

    Shared Sub FillStyles(ByRef strRtf As String)
        Dim strStiler As String
        Dim strStilDef As String
        Dim strStilNavn As String
        Dim intStilNr As Integer
        Dim lngStart As Integer
        Dim lngSlutt As Integer
        htStyles = New Hashtable()

        Dim i1 As Integer = strRtf.IndexOf("{\stylesheet", 0)
        Dim i2 As Integer = strRtf.IndexOf("}}", i1)
        Dim strStyles As String = strRtf.Substring(i1, i2 - i1 + 2)

        lngStart = InStr(1, strStyles, "{\stylesheet")
        If lngStart = 0 Then
            '*** RTF-Stil seksjonen ikke funnet, avslutter rutine.
            Exit Sub
        End If
        lngSlutt = InStr(lngStart, strStyles, "}}") + 2
        strStiler = Mid(strStyles, lngStart, lngSlutt - lngStart)

        lngStart = InStr(10, strStiler, "{\s")
        Do Until lngStart = 0
            lngSlutt = InStr(lngStart, strStiler, ";}") + 2
            strStilDef = Mid(strStiler, lngStart, lngSlutt - lngStart)

            HentStilNrNavn(strStilDef, intStilNr, strStilNavn)

            strStilNavn = LCase(strStilNavn)
            'strStilNavn = Replace(strStilNavn, "_", "")
            strStilNavn = Replace(strStilNavn, " ", "")
            strStilNavn = Replace(strStilNavn, "/", "")
            strStilNavn = Replace(strStilNavn, "\", "")
            strStilNavn = Replace(strStilNavn, "(", "")
            strStilNavn = Replace(strStilNavn, ")", "")
            strStilNavn = Replace(strStilNavn, "&", ".")
            strStilNavn = Replace(strStilNavn, ":", ".")
            strStilNavn = Replace(strStilNavn, "�", "o")
            strStilNavn = Replace(strStilNavn, "�", "a")
            strStilNavn = Replace(strStilNavn, "�", "a")

            If Not strStilNavn.StartsWith("heading") Then
                While Char.IsDigit(strStilNavn.Substring(strStilNavn.Length - 1, 1))
                    ' Strips off trailing numbers on stylenames
                    strStilNavn = strStilNavn.Substring(0, strStilNavn.Length - 1)
                End While
            End If

            htStyles.Add(intStilNr, strStilNavn)
            'arrStiler(1, intStilNr) = strStilDef
            lngStart = InStr(lngSlutt, strStiler, "{\s")
        Loop
    End Sub

    Shared Sub HentStilNrNavn(ByRef strStilDef As String, ByRef intStilNr As Short, ByRef strStilNavn As String)
        Dim lngStart As Integer
        Dim lngSlutt As Integer
        Dim temp As String

        intStilNr = Val(Mid(strStilDef, 4, 4))

        temp = Right(strStilDef, 30)

        lngStart = InStr(1, strStilDef, "\snext")
        If lngStart = 0 Then
            '*** RTF-Stil seksjonen ikke funnet, avslutter rutine.
            Exit Sub
        End If
        lngStart = InStr(lngStart, strStilDef, " ") + 1
        lngSlutt = InStr(lngStart, strStilDef, ";}")

        strStilNavn = Mid(strStilDef, lngStart, lngSlutt - lngStart)

    End Sub

End Class
